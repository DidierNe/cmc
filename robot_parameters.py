"""Robot parameters"""

import numpy as np
from farms_core import pylog

class RobotParameters(dict):
    """Robot parameters"""

    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__

    def __init__(self, parameters):
        super(RobotParameters, self).__init__()

        # Initialise parameters
        self.n_body_joints = parameters.n_body_joints
        self.n_legs_joints = parameters.n_legs_joints
        self.initial_phases = parameters.initial_phases
        self.n_joints = self.n_body_joints + self.n_legs_joints
        self.n_oscillators_body = 2*self.n_body_joints
        self.n_oscillators_legs = self.n_legs_joints
        self.n_oscillators = self.n_oscillators_body + self.n_oscillators_legs
        self.freqs = np.zeros(self.n_oscillators)
        self.coupling_weights = np.zeros([
            self.n_oscillators,
            self.n_oscillators,
        ])
        self.phase_bias = np.zeros([self.n_oscillators, self.n_oscillators])
        self.rates = np.zeros(self.n_oscillators)
        self.nominal_amplitudes = np.zeros(self.n_oscillators)
        self.feedback_gains_swim = np.zeros(self.n_oscillators)
        self.feedback_gains_walk = np.zeros(self.n_oscillators)
        # Gains for final motor output
        self.position_body_gain = parameters.position_body_gain
        self.position_limb_gain = parameters.position_limb_gain
        self.phase_lag_body_limb=parameters.phase_lag_body_limb
        self.amplitude_gradient=parameters.amplitude_gradient
        
        # Drive
        self.drive = parameters.drive

        self.update(parameters)

    def update(self, parameters):
        """Update network from parameters"""
        self.drive = parameters.drive
        self.set_frequencies(parameters)  # f_i
        self.set_coupling_weights(parameters)  # w_ij
        self.set_phase_bias(parameters)  # phi_ij
        self.set_amplitudes_rate(parameters)  # a_i
        self.set_nominal_amplitudes(parameters)  # R_i

    def step(self, iteration, salamandra_data):
        """Step function called at each iteration

        Parameters
        ----------

        salamanra_data: salamandra_simulation/data.py::SalamandraData
            Contains the robot data, including network and sensors.

        gps (within the method): Numpy array of shape [9x3]
            Numpy array of size 9x3 representing the GPS positions of each link
            of the robot along the body. The first index [0-8] coresponds to
            the link number from head to tail, and the second index [0,1,2]
            coressponds to the XYZ axis in world coordinate.

        
        gps = np.array(
            salamandra_data.sensors.links.urdf_positions()[iteration, :9],
        )
        # print("GPGS: {}".format(gps[4, 0]))
        # print("drive: {}".format(self.sim_parameters.drive))
        if gps[4,0] >= 4.866:
            self.drive = 4
            self.set_frequencies(self.drive)
            self.set_nominal_amplitudes(self.drive)
        else:
            self.drive = 2
            self.set_frequencies(self.drive)
            self.set_nominal_amplitudes(self.drive)
"""
    def set_frequencies(self, parameters):
        """Set frequencies"""
        # f_i = frequency of CPG i
        # Body oscillator
        for i in range(self.n_oscillators_body):
            if 1. <= self.drive <= 5.:
                self.freqs[i] = 0.2*self.drive + 0.3
            else:       # when saturation
                self.freqs[i] = 0    
        # Limb oscillator
        for i in range(self.n_oscillators_body, self.n_oscillators):
            if 1. <= self.drive <= 3.:
                self.freqs[i] = 0.2*self.drive
            else:       # when saturation
                self.freqs[i] = 0 

    def set_coupling_weights(self, parameters):
        """Set coupling weights"""
        # w_ij = coupling weight from CPG i to CPG j
        low_coupling_weights = 10
        high_coupling_weights = 30
        # Between body oscillators
        for i in range(self.n_oscillators_body//2-1):
            self.coupling_weights[i,i+1] = low_coupling_weights # from CPGs 0:6 to 1:7
            self.coupling_weights[i+1,i] = low_coupling_weights # from CPGs 1:7 to 0:6
            self.coupling_weights[i+8,i+9] = low_coupling_weights   # from CPGs 8:14 to 9:15
            self.coupling_weights[i+9,i+8] = low_coupling_weights   # from CPGs 9:15 to 8:14
        for i in range(self.n_oscillators_body//2):
            self.coupling_weights[i,i+8] = low_coupling_weights # from CPGs 0:7 to 8:15
            self.coupling_weights[i+8,i] = low_coupling_weights # from CPGs 8:15 to 0:7
        # Between limb and body oscillators
        for i in range(self.n_oscillators_legs//2):
            for j in range(self.n_oscillators_legs):
                self.coupling_weights[j+4*i,16+2*i] = high_coupling_weights # from CPGs 16 & 18 to 0:3 & 4:7
                self.coupling_weights[j+8+4*i,17+2*i] = high_coupling_weights   # from CPGs 17 & 19 to 8:11 & 12:15
        # Between limb oscillators
        for i in [self.n_oscillators_body,self.n_oscillators-1]:
            for j in [self.n_oscillators_body+1,self.n_oscillators-2]:
                self.coupling_weights[i,j] = low_coupling_weights
                self.coupling_weights[j,i] = low_coupling_weights    

    def set_phase_bias(self, parameters):
        """Set phase bias"""
        # phi_ij = phase bias from CPG j to CPG i
        low_phase_bias = np.pi/4
        high_phase_bias = np.pi
        # Between body oscillators
        for i in range(self.n_oscillators_body//2-1):
            self.phase_bias[i,i+1] = -low_phase_bias + parameters.phase_lag_body[i]+parameters.phase_lag_body[i+1] # from CPGs 0:6 to 1:7
            self.phase_bias[i+1,i] = low_phase_bias + parameters.phase_lag_body[i]+parameters.phase_lag_body[i+1] # from CPGs 1:7 to 0:6
            self.phase_bias[i+8,i+9] = -low_phase_bias + parameters.phase_lag_body[i+8]+parameters.phase_lag_body[i+9]  # from CPGs 8:14 to 9:15
            self.phase_bias[i+9,i+8] = low_phase_bias + parameters.phase_lag_body[i+9]+parameters.phase_lag_body[i+8]   # from CPGs 9:15 to 8:14
        for i in range(self.n_oscillators_body//2):
            self.phase_bias[i,i+8] = high_phase_bias + parameters.phase_lag_body[i]+parameters.phase_lag_body[i+8]# from CPGs 0:7 to 8:15
            self.phase_bias[i+8,i] = high_phase_bias + parameters.phase_lag_body[i]+parameters.phase_lag_body[i+8]# from CPGs 8:15 to 0:7

        # Between limb and body oscillators
        for i in range(self.n_oscillators_legs//2):
            for j in range(self.n_oscillators_legs):
                self.phase_bias[j+4*i,16+2*i] = high_phase_bias+ self.phase_lag_body_limb # from CPGs 16 & 18 to 0:3 & 4:7
                self.phase_bias[j+8+4*i,17+2*i] = high_phase_bias + self.phase_lag_body_limb   # from CPGs 17 & 19 to 8:11 & 12:15
        # Between limb oscillators
        for i in [self.n_oscillators_body,self.n_oscillators-1]:
            for j in [self.n_oscillators_body+1,self.n_oscillators-2]:
                self.phase_bias[i,j] = high_phase_bias + parameters.phase_lag_body[i]+parameters.phase_lag_body[j]
                self.phase_bias[j,i] = high_phase_bias + parameters.phase_lag_body[j]+parameters.phase_lag_body[i]

    
    def set_amplitudes_rate(self, parameters):
        """Set amplitude rates"""
        # a_i = amplitude rate of CPG i
        for i in range(self.n_oscillators):
            self.rates[i] = 20

    def set_nominal_amplitudes(self, parameters):
        """Set nominal amplitudes"""
        # R_i = nominal amplitude of CPG i

        if self.amplitude_gradient is not None:
            for i in range(self.n_oscillators_body):
                self.nominal_amplitudes[i] = self.amplitude_gradient
        else:
            for i in range(self.n_oscillators_body):
                if 1. <= self.drive <= 5.:
                    self.nominal_amplitudes[i] = 0.065*self.drive + 0.196
                else:       # when saturation
                    self.nominal_amplitudes[i] = 0    
        
        
        # Limb oscillator
        for i in range(self.n_oscillators_body, self.n_oscillators):
            if 1. <= self.drive <= 3.:
                self.nominal_amplitudes[i] = 0.131*self.drive+ 0.131
            else:       # when saturation
                self.nominal_amplitudes[i] = 0


# class RobotParameters(dict):
#     """Robot parameters"""

#     __getattr__ = dict.__getitem__
#     __setattr__ = dict.__setitem__

#     def __init__(self, parameters):
#         super(RobotParameters, self).__init__()

#         # Initialise parameters
#         self.n_body_joints = parameters.n_body_joints
#         self.n_legs_joints = parameters.n_legs_joints
#         self.initial_phases = parameters.initial_phases
#         self.n_joints = self.n_body_joints + self.n_legs_joints
#         self.n_oscillators_body = 2*self.n_body_joints
#         self.n_oscillators_legs = self.n_legs_joints
#         self.n_oscillators = self.n_oscillators_body + self.n_oscillators_legs
#         self.freqs = np.zeros(self.n_oscillators)
#         self.coupling_weights = np.zeros([
#             self.n_oscillators,
#             self.n_oscillators,
#         ])
#         self.phase_bias = np.zeros([self.n_oscillators, self.n_oscillators])
#         self.rates = np.zeros(self.n_oscillators)
#         self.nominal_amplitudes = np.zeros(self.n_oscillators)
#         self.feedback_gains_swim = np.zeros(self.n_oscillators)
#         self.feedback_gains_walk = np.zeros(self.n_oscillators)

#         # gains for final motor output
#         self.position_body_gain = parameters.position_body_gain
#         self.position_limb_gain = parameters.position_limb_gain

#         self.update(parameters)

#     def update(self, parameters):
#         """Update network from parameters"""
#         self.set_frequencies(parameters)  # f_i
#         self.set_coupling_weights(parameters)  # w_ij
#         self.set_phase_bias(parameters)  # phi_ij
#         self.set_amplitudes_rate(parameters)  # a_i
#         self.set_nominal_amplitudes(parameters)  # R_i

#     def step(self, iteration, salamandra_data):
#         """Step function called at each iteration

#         Parameters
#         ----------

#         salamanra_data: salamandra_simulation/data.py::SalamandraData
#             Contains the robot data, including network and sensors.

#         gps (within the method): Numpy array of shape [9x3]
#             Numpy array of size 9x3 representing the GPS positions of each link
#             of the robot along the body. The first index [0-8] coressponds to
#             the link number from head to tail, and the second index [0,1,2]
#             coressponds to the XYZ axis in world coordinate.

#         """
#         gps = np.array(
#             salamandra_data.sensors.links.urdf_positions()[iteration, :9],
#         )
#         # print("GPGS: {}".format(gps[4, 0]))
#         # print("drive: {}".format(self.sim_parameters.drive))

#     def set_frequencies(self, parameters):
#         """Set frequencies"""
#         pylog.warning(
#             'Set the frequencies of the spinal and limb oscillators as a function of the drive')

#     def set_coupling_weights(self, parameters):
#         """Set coupling weights"""
#         pylog.warning('Coupling weights must be set')

#     def set_phase_bias(self, parameters):
#         """Set phase bias"""
#         pylog.warning('Phase bias must be set')

#     def set_amplitudes_rate(self, parameters):
#         """Set amplitude rates"""
#         pylog.warning('Convergence rates must be set')

#     def set_nominal_amplitudes(self, parameters):
#         """Set nominal amplitudes"""
#         pylog.warning('Nominal amplitudes must be set')
