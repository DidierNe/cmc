"""[Project1] Exercise 2: Swimming & Walking with Salamander Robot"""

import os
import pickle
import numpy as np
from salamandra_simulation.simulation import simulation
from simulation_parameters import SimulationParameters
import matplotlib.pyplot as plt
from plot_results import compute_speed
from plot_results import plot_positions
from plot_results import plot_trajectory
from plot_results import plot_2d
from plot_results import sum_torques
from salamandra_simulation.save_figures import save_figures
import farms_pylog as pylog


def exercise_3a_coordination(timestep):
    """[Project 1] Exercise 3a Limb and Spine coordination

    This exercise explores how phase difference between spine and legs
    affects locomotion.

    Run the simulations for different walking drives and phase lag between body
    and leg oscillators.

    """
    # Use exercise_example.py for reference
    parameter_set = [
        SimulationParameters(
            duration=10,  # Simulation duration in [s]
            timestep=timestep,  # Simulation timestep in [s]
            spawn_position=[0, 0, 0.1],  # Robot position in [m]
            spawn_orientation=[0, 0, 0],  # Orientation in Euler angles [rad]
            drive= drive,  # An example of parameter part of the grid search
            amplitudes=1,  # Just an example
             #amplitudes=[1, 2, 3],  # Just an example
            phase_lag_body_limb= phase_lag_body_limb,  # or np.zeros(n_joints) for example
            turn=0,  # Another example
         
        )
        for drive in np.linspace(1, 3,5)
        
        for phase_lag_body_limb in np.linspace(-1.5,1.5,5) #varying for each drive the phase lag
       
    ]

    os.makedirs('./logs/ex3a/', exist_ok=True)
    record = np.zeros((len(parameter_set),3))
    for simulation_i, sim_parameters in enumerate(parameter_set):
        filename = './logs/ex3a/simulation_{}.{}'
        sim, data = simulation(
            sim_parameters=sim_parameters,  # Simulation parameters, see above
            arena='land',  # Can also be 'land', give it a try!
            fast=False,  # For fast mode (not real-time)
            headless=True,  # For headless mode (No GUI, could be faster)
            record=False,  # Record video
            record_path="videos/ex3a/test{}_video_drive_{}_phaseLag_B/L_{}_".format(simulation_i,sim_parameters.drive,sim_parameters.phase_lag_body_limb),  # video savging path
            camera_id=0  # camera type: 0=top view, 1=front view, 2=side view,
        )
        timestep = data.timestep
        n_iterations = np.shape(data.sensors.links.array)[0]
        times = np.arange(
            start=0,
            stop=timestep*n_iterations,
            step=timestep,
        )
        timestep = times[1] - times[0]
        links_positions = data.sensors.links.urdf_positions()
        head_positions = links_positions[:, 0, :]
        joints_torques = data.sensors.joints.motor_torques_all()
        links_vel = np.array(data.sensors.links.com_lin_velocities())

        
        vel,lat=compute_speed(np.array(links_positions), np.array(links_vel), n_iterations)
        #inside the for loop, thus record uptade
        record[simulation_i,:] = [sim_parameters.drive,sim_parameters.phase_lag_body_limb,vel]




        # Log robot data
        data.to_file(filename.format(simulation_i, 'h5'), sim.iteration)
        # Log simulation parameters
        with open(filename.format(simulation_i, 'pickle'), 'wb') as param_file:
            pickle.dump(sim_parameters, param_file)

        #Uncomment the following paragraph if you want to obtain the maximum value of the speed and the corresponding phase body/limb lag.
        '''
        # Extract the first column
        first_column = record[:, 0]

        # Find unique values in the first column
        unique_values = np.unique(first_column)

        # Iterate over the unique values
        for unique_value in unique_values:
            # Filter rows where the first column matches the unique value
            matching_rows = record[record[:, 0] == unique_value]
            
            # Find the index of the maximum value in the third column
            max_index = np.argmax(matching_rows[:, 2])
            
            # Extract the corresponding value from the second column
            max_second_column = matching_rows[max_index, 1]
            
            # Extract the maximum value from the third column
            maximum = matching_rows[max_index, 2]
            
            # Print the maximum and the corresponding value from the second column
            print("For a drive of", unique_value, ":")
            print("Max speed reached for a fixed drive:", maximum)
            print("Phase lag body limb for the max:", max_second_column)
            print()
        '''
    
    labels=['drive','phase lag body limb','speed forward']
    plot_2d(record,labels)
    plt.title('looking for the highest velocity')
    save_figures()
    plt.show()
    plt.figure('2D_phase_lag_body_limb')

#Uncomment the following paragraph if you want to plot the speed for differents amplitude for a fixed frive
"""     plt.plot(record[:, 1],record[:, 2])
    plt.xlabel('Lag between limb and body [rad]')
    plt.ylabel('mean forward speed [m/s]')
    plt.title("Drive constant of: {}".format(sim_parameters.drive))
    plt.grid(True)
    plt.show() """
    



def exercise_3b_coordination(timestep):
    """[Project 1] Exercise 3b Limb and Spine coordination

    This exercise explores how spine amplitude affects coordination.

    Run the simulations for different walking drives and body amplitude.

    """
    
    parameter_set = [
        SimulationParameters(
            duration=10,  # Simulation duration in [s]
            timestep=timestep,  # Simulation timestep in [s]
            spawn_position=[0, 0, 0.1],  # Robot position in [m]
            spawn_orientation=[0, 0, 0],  # Orientation in Euler angles [rad]
            drive=drive,  # An example of parameter part of the grid search
            amplitudes=1,  # Just an example
            phase_lag_body_limb= -1.17,  # optimal value from 3a
            turn=0,  # Another example
            amplitude_gradient =amplitude_gradient #this variable is used to vary the amplitude, a new one can be created if there is a conflict in the future
        )
        for drive in np.linspace(1, 3, 10)
        #varying for each drive the phase lag
            for amplitude_gradient in np.linspace(0,2*(0.131*drive+ 0.131),20) #varying amplitude (exceeds saturation)
        # for ...
    ]

    os.makedirs('./logs/ex3_p_b/', exist_ok=True)
    record = np.zeros((len(parameter_set),3))
    for simulation_i, sim_parameters in enumerate(parameter_set):
        filename = './logs/ex3b/simulation_{}.{}'
        sim, data = simulation(
            sim_parameters=sim_parameters,  # Simulation parameters, see above
            arena='land',  # Can also be 'land', give it a try!
            fast=True,  # For fast mode (not real-time)
            headless=True,  # For headless mode (No GUI, could be faster)
            record=True,  # Record video
            record_path="videos/ex3b/test{}_video_drive_{}_oscillation_amplitude_{}_".format(simulation_i,sim_parameters.drive,sim_parameters.amplitude_gradient),  # video savging path
            camera_id=0  # camera type: 0=top view, 1=front view, 2=side view,
        )
        # Log robot data
        data.to_file(filename.format(simulation_i, 'h5'), sim.iteration)
        # Log simulation parameters
        with open(filename.format(simulation_i, 'pickle'), 'wb') as param_file:
            pickle.dump(sim_parameters, param_file)    
        timestep = data.timestep
        n_iterations = np.shape(data.sensors.links.array)[0]
        times = np.arange(
            start=0,
            stop=timestep*n_iterations,
            step=timestep,
        )
        timestep = times[1] - times[0]
        links_positions = data.sensors.links.urdf_positions()
        head_positions = links_positions[:, 0, :]
        joints_torques = data.sensors.joints.motor_torques_all()
        links_vel = np.array(data.sensors.links.com_lin_velocities())

        
        vel,lat=compute_speed(np.array(links_positions), np.array(links_vel), n_iterations)
        #inside the for loop, thus record uptade
        record[simulation_i,:] = [sim_parameters.drive,sim_parameters.amplitude_gradient,vel]



        #Uncomment the following paragraph if you want to obtain the maximum value of the speed and the corresponding phase body/limb lag.
        # Extract the first column
        first_column = record[:, 0]

        # Find unique values in the first column
        unique_values = np.unique(first_column)

        # Iterate over the unique values
        for unique_value in unique_values:
            # Filter rows where the first column matches the unique value
            matching_rows = record[record[:, 0] == unique_value]
            
            # Find the index of the maximum value in the third column
            max_index = np.argmax(matching_rows[:, 2])
            
            # Extract the corresponding value from the second column
            max_second_column = matching_rows[max_index, 1]
            
            # Extract the maximum value from the third column
            maximum = matching_rows[max_index, 2]
            
            # Print the maximum and the corresponding value from the second column
            print("For a drive of", unique_value, ":")
            print("Max speed reached for a fixed drive:", maximum)
            print("Phase lag body limb for the max:", max_second_column)
            print()


    labels=['drive','amplitude oscillation','speed forward']
    plt.figure('2D_amplitude_oscillation')
    plot_2d(record,labels)
    plt.figure('2D_amplitude_body')

    save_figures()
    plt.show()

    #Uncomment the following paragraph if you want to plot the speed for differents amplitude for a fixed frive
"""     plt.plot(record[:, 1],record[:, 2])
    plt.xlabel('Amplitude in body oscillator [m]')
    plt.ylabel('forward speed [m/s]')
    plt.title("Drive constant of: {}".format(sim_parameters.drive))
    plt.grid(True)
    plt.show() """
 



  


if __name__ == '__main__':
    # exercise_3a_coordination(timestep=1e-2)
    exercise_3b_coordination(timestep=1e-2)

